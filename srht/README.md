Code in this package and its sub-packages is auto-generated with gqlclientgen.
To add or change a GraphQL query, edit the `.graphql` file and then run:

    go generate ./...
