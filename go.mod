module git.sr.ht/~xenrox/hut

go 1.17

require (
	git.sr.ht/~emersion/go-scfg v0.0.0-20240128091534-2ae16e782082
	git.sr.ht/~emersion/gqlclient v0.0.0-20230820050442-8873fe0204b9
	github.com/dustin/go-humanize v1.0.1
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/juju/ansiterm v1.0.0
	github.com/spf13/cobra v1.8.0
	golang.org/x/term v0.20.0
)

require (
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/dave/jennifer v1.7.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/lunixbochs/vtclean v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/vektah/gqlparser/v2 v2.5.8 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
