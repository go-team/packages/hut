# [hut]

[![builds.sr.ht status](https://builds.xenrox.net/~xenrox/hut/commits/master.svg)](https://builds.xenrox.net/~xenrox/hut/commits/master?)

A CLI tool for [sr.ht].

## Usage

Run `hut init` to get started. Read the man page to learn about all commands.

## Building

Dependencies:

- Go
- scdoc (optional, for man pages)

For end users, a `Makefile` is provided:

    make
    sudo make install

## Contributing

Send patches to the [mailing list], report bugs on the [issue tracker].

Join the IRC channel: [#hut on Libera Chat].

## License

AGPLv3 only, see [LICENSE].

Copyright (C) 2021 Simon Ser

[hut]: https://sr.ht/~xenrox/hut/
[sr.ht]: https://sr.ht/~sircmpwn/sourcehut/
[mailing list]: https://lists.sr.ht/~xenrox/hut-dev
[issue tracker]: https://todo.sr.ht/~xenrox/hut
[#hut on Libera Chat]: ircs://irc.libera.chat/#hut
[LICENSE]: LICENSE
